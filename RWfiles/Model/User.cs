﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWfiles.Model
{
    internal class User
    {
        public string UserName { get; set; }    
        public string Password { get; set; }

        public override string ToString()
        {
            return this.UserName + ":" + this.Password; 
        }
    }
}
