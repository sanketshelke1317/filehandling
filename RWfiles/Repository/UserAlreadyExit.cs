﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWfiles.Repository
{
    internal class UserAlreadyExit:Exception
    {
        public UserAlreadyExit()
        {

        }
        public UserAlreadyExit(string message):base(message)
        {
        }
    }
}
