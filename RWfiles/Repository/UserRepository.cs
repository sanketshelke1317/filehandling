﻿using RWfiles.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWfiles.Repository
{
    internal class UserRepository
    {
        internal void addUser(string userDetails, User user)
        {
            StreamWriter sw = new StreamWriter(userDetails, true);


                sw.WriteLine(user);
                sw.Close();
            
        }

        internal void isUserExist(string userDetails, User user)
        {
            StreamReader sr = new StreamReader(userDetails, true);
            
                string str = sr.ReadToEnd();
                if (str.Contains(user.UserName))
                {
                throw (new UserAlreadyExit("User Already Exist"));
                    //Console.WriteLine("User Already Exists");
                }
                else
                {
                    sr.Close();
                    addUser(userDetails, user);
                    Console.WriteLine("Register Successful!");
                }
            
        }
    }
}
